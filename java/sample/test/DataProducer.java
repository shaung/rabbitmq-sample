package test;

import mq.*;
import service.*;

public class DataProducer {

    public static void main(String []args) throws Exception {
        final String exchangeName = "my_exchange";
        final String routingKey = "pdf_convert";

        MQ mq = new MQ();
        mq.send(exchangeName, routingKey, "hi!");
    }
}


package mq;

import com.rabbitmq.client.*;

public class Message {
    public QueueingConsumer.Delivery delivery;

    public Message(QueueingConsumer.Delivery delivery) {
        this.delivery = delivery;
    }

    public Envelope getEnvelope() {
        return delivery.getEnvelope();
    }

    public AMQP.BasicProperties getProperties() {
        return delivery.getProperties();
    }

    public byte[] getBody() {
        return delivery.getBody();
    }
}

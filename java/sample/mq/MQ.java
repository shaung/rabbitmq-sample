package mq;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.*;
import mq.*;

public class MQ {
    public interface Job {
        public void doJob(Chan ch, Message msg);
    }

    public static ConnectionFactory getFactory() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setVirtualHost("/");
        factory.setHost("127.0.0.1");
        factory.setPort(5672);
        return factory;
    }

    public void consume(String exchangeName, String queueName, String routingKey, Job job) throws Exception {
        Connection conn = getFactory().newConnection();
        Channel ch = conn.createChannel();
        Chan chan = new Chan(ch);
        boolean durable = true;

        ch.exchangeDeclare(exchangeName, "direct", durable);
        ch.queueDeclare(queueName, durable, false, false, null);
        ch.queueBind(queueName, exchangeName, routingKey);
        boolean noAck = false;

        QueueingConsumer consumer = new QueueingConsumer(ch);
        ch.basicConsume(queueName, noAck, consumer);

        boolean runInfinite = true;
        while (runInfinite) {
            QueueingConsumer.Delivery delivery;
            try {
                // 抓取消息
                delivery = consumer.nextDelivery();
            } catch (InterruptedException ie) {
                continue;
            }
            System.out.println("[Message received] " + new String(delivery.getBody()));
            // 收到谢谢
            ch.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            // 处理
            Message msg = new Message(delivery);
            job.doJob(chan, msg);
        }
        ch.close();
        conn.close();
    }

    public void send(String exchangeName, String routingKey, String msg) throws Exception {
        Connection conn = getFactory().newConnection();
        Channel ch = conn.createChannel();
        boolean durable = true;

        ch.exchangeDeclare(exchangeName, "direct", durable);

        byte[] messageBodyBytes = msg.getBytes();
        ch.basicPublish(exchangeName, routingKey, MessageProperties.PERSISTENT_TEXT_PLAIN, messageBodyBytes) ;

        ch.close();
        conn.close();
    }
}

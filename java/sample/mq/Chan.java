package mq;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.*;

public class Chan {
    private Channel channel = null;

    public Chan(Channel ch) {
        channel = ch;
    }

    public void send(String exchangeName, String routingKey, String messageBody) throws Exception {
        byte[] messageBodyBytes = messageBody.getBytes();
        channel.basicPublish(exchangeName, routingKey, MessageProperties.PERSISTENT_TEXT_PLAIN, messageBodyBytes);
    }
}


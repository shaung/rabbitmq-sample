package service;

import mq.*;

public class PDFConvertor {

    public static void main(String []args) throws Exception {
        final String exchangeName = "my_exchange";
        final String routingKey = "pdf_convert";

        MQ.Job job = new MQ.Job() {
            public void doJob(Chan ch, Message msg) {
                String body = new String(msg.getBody());
                // Process 
                System.out.println("Processing..." + body);
                System.out.println("Generated. Ready to notice printing service");
                // ...

                // notice other services when done
                String messageBody = "[document to print] " + body;
                try {
                    ch.send(exchangeName, "print_service", messageBody) ;
                } catch (Exception e) {
                    // FIXME: Need to be handled properly.
                    System.out.println(e.toString());
                }
            }
        };

        MQ mq = new MQ();
        mq.consume(exchangeName, "pdf_queue", routingKey, job);
    }
}

package service;

import mq.*;

public class PrintService {

    public static void main(String []args) throws Exception {
        final String exchangeName = "my_exchange";
        final String routingKey = "print_service";

        MQ.Job job = new MQ.Job() {
            public void doJob(Chan ch, Message msg) {
                String body = new String(msg.getBody());
                // Process 
                System.out.println("Printing..." + body);
                System.out.println("Printing... done");
                // ...

                // notice other services when done
                String messageBody = "print done!";
                try {
                    ch.send(exchangeName, "results", messageBody) ;
                } catch (Exception e) {
                    // FIXME: Need to be handled properly.
                    System.out.println(e.toString());
                }
            }
        };

        MQ mq = new MQ();
        mq.consume(exchangeName, "print_queue", routingKey, job);
    }
}

import com.rabbitmq.client.*;

public final class Common {
    private static ConnectionFactory getFactory() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setVirtualHost("/");
        factory.setHost("127.0.0.1");
        factory.setPort(5672);
        return factory;
    }

    public static Connection getConnection() throws Exception {
        Connection conn = getFactory().newConnection();
        return conn;
    }
}

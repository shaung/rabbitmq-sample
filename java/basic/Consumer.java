import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.*;

public class Consumer {
    public static void main(String []args) throws Exception {
        Connection conn = Common.getConnection();
        Channel ch = conn.createChannel();

        String exchangeName = "basic-sample";
        String queueName = "foo-queue";
        String routingKey = "echo";
        boolean durable = true;
        boolean noAck = false;

        ch.exchangeDeclare(exchangeName, "direct", durable);
        ch.queueDeclare(queueName, durable, false, false, null);
        ch.queueBind(queueName, exchangeName, routingKey);

        QueueingConsumer consumer = new QueueingConsumer(ch);
        ch.basicConsume(queueName, noAck, consumer);

        boolean serveForEver = true;
        while (serveForEver) {
            QueueingConsumer.Delivery delivery;
            try {
                // 抓取消息
                delivery = consumer.nextDelivery();
            } catch (InterruptedException ie) {
                continue;
            }
            // 处理
            System.out.println(new String(delivery.getBody()));
            // 收到谢谢
            ch.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            // 处理完成，给下个队列发消息
        }

        ch.close();
        conn.close();
    }
}

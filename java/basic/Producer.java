import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.*;

public class Producer {
   public static void main(String []args) throws Exception {
        Connection conn = Common.getConnection();
        Channel ch = conn.createChannel();

        String exchangeName = "basic-sample";
        String routingKey = "echo";

        for (String s: args) {
            byte[] messageBodyBytes = s.getBytes();
            ch.basicPublish(exchangeName, routingKey, MessageProperties.PERSISTENT_TEXT_PLAIN, messageBodyBytes) ;
            System.out.println("sent: " + s);
        }

        ch.close();
        conn.close();
    }
}

